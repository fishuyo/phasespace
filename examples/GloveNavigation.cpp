/*
 * Tim Wood 
 * */

#include <iostream>
#include <vector>
#include "allocore/al_Allocore.hpp"
#include "allocore/io/al_App.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/Glove.hpp"
#include "phasespace/DebugDraw.hpp"

int main() {}

using namespace std;
using namespace al;

#define PINCH_JOYSTICK 0
#define SPACEPULL 1

struct Box {
  Pose pose;
  Vec3f scale;
  Mesh m;

  bool hover;
  bool selected;

  void draw(Graphics &g){
    g.pushMatrix();
    g.translate(pose.pos());
    g.rotate(pose.quat());
    g.scale(scale);
    if(selected) g.color(1,0,1);
    else if(hover) g.color(0,1,1);
    else g.color(1,0,0);
    g.draw(m);
    g.popMatrix();
  }
};


struct GloveNavigationTest : App, Phasespace::Listener {

  Material material;
  Light light;

  vector<Box*> boxes;

  int mode = SPACEPULL;
  float sensitivity = 1;

  GloveNavigationTest(){

    // create and get PhaspaceManger master instance
    Phasespace::master()->addListener(*this)->startPlaybackFile("Phasespace/marker-data/gloves.txt");
    // Phasespace::master()->addListener(*this)->start();

    // set up environment of random rectangular prisms
    for( int i=-20; i < 20; i++){
      // Mesh *m = new Mesh();
      Box *b = new Box();
      b->m.primitive(Graphics::TRIANGLES);
      addCube(b->m);
      float sy = rnd::uniform();
      b->pose.pos().set( i*.5f, sy*.5f, -.5f + 1.f*(i%2) );
      b->scale.set( rnd::uniform(), sy, rnd::uniform() );
      b->m.generateNormals();
      boxes.push_back(b);
    }

    nav().pos(0,0,5);
    
    ViewpointWindow* win = initWindow(Window::Dim(700, 700));
    start();
  }
  
  virtual void onPhasespaceEvent(const Phasespace::Event &event) {
    
    static Nav oldNav;

    Glove l = event.leftGlove;

    switch(mode){
      case PINCH_JOYSTICK:
        // Navigation joystick mode
        // Translation done with the left index finger pinch gesture
        if( l.pinchOn[eIndex]){
        } else if( l.pinched[eIndex]){
          Vec3f translate = sensitivity * l.getPinchTranslate(eIndex);
          for(int i=0; i<3; i++){
            nav().pos()[i] = nav().pos()[i] * 0.9 + 
              (nav().pos()[i] + translate.dot(Vec3d(nav().ur()[i], nav().uu()[i], -nav().uf()[i]))) * 0.1;
          }
          // nav().pos().lerp( nav().pos() + translate, 0.01f);
        } else if( l.pinchOff[eIndex] ){
        } else {
        }

      case SPACEPULL:
        // Navigation space pulling
        // Translation done with the left index finger pinch gesture
        if( l.pinchOn[eIndex]){
          oldNav.pos().set(nav().pos());
        } else if( l.pinched[eIndex]){
          Vec3f translate = sensitivity * l.getPinchTranslate(eIndex);
          for(int i=0; i<3; i++){
            nav().pos()[i] = nav().pos()[i] * 0.9 + 
              (oldNav.pos()[i] - translate.dot(Vec3d(nav().ur()[i], nav().uu()[i], -nav().uf()[i]))) * 0.1;
          }
          // nav().pos().lerp( oldNav.pos() - translate, 0.1f);
        } else if( l.pinchOff[eIndex] ){
          Vec3f vel = l.getPinchVelocity(eIndex);
          vel.z *= -1;
          nav().move(-vel * sensitivity * 0.01); // set velocity
        } else {
          nav().move( nav().move()*0.8); // decay velocity
        }
    }

    // set navigation sensitivity with middle finger invisible slider gesture
    if( l.pinched[eMiddle]){
      Vec3f middle = l.getPinchTranslate(eMiddle);
      sensitivity = abs(middle.y*10);
    }

  }


  virtual void onDraw(Graphics& g, const Viewpoint& v) {
    material();
    light();
    
    g.antialiasing(Graphics::NICEST);

    DebugDraw::drawGloves(g);

    for(int i=0; i < boxes.size(); i++) boxes[i]->draw(g);
  }

};

// Make an app
//
GloveNavigationTest app;
