/*
 * RigidBodyAuto.cpp - receive marker and rigid body data from
 * auto detected rigid body.
 * Tim Wood 2013 
 * */

#include <iostream>
#include "allocore/al_Allocore.hpp"
#include "allocore/io/al_App.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/DebugDraw.hpp"

int main() {}

using namespace std;
using namespace al;

struct RigidBodyAutoApp : App {

  Phasespace *tracker;

  RigidBodyAutoApp(){

    // create and get Phaspace master instance
    tracker = Phasespace::master();

    // start tracker data listener thread
    tracker->start(); 

    // auto detect rigid body form visible markers and restart
    int ret = 0;
    int tries = 5;
    while( ret == 0 && tries-- > 0){
      cout << "detecting rigid from visible markers... (" << tries << " more tries)\n";
      sleep(1);
      ret = tracker->addRigidBodyFromVisibleMarkers();
    }
    cout << "rigid body added using " << ret << " markers, restarted owl server.\n";

    nav().pos(0,0,5);
    
    ViewpointWindow* win = initWindow(Window::Dim(700, 700));
    start();
  }
  
  virtual void onAnimate(double dt) {}

  virtual void onDraw(Graphics& g, const Viewpoint& v) {

    g.antialiasing(Graphics::NICEST);

    DebugDraw::drawAlloSphere(g);
    DebugDraw::drawMarkers(g);
    DebugDraw::drawRigid(g,0);
  }

};

// Make an app
//
RigidBodyAutoApp app;
