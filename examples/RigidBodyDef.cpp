/*
 * RigidBodyDef.cpp - receive marker and rigid body data for hard coded,
 * hand measured rigid body definition.
 *
 * Tim Wood 2013 
 * */

#include <iostream>
#include "allocore/al_Allocore.hpp"
#include "allocore/io/al_App.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/DebugDraw.hpp"

int main() {}

using namespace std;
using namespace al;

#define RIGID_MARKER_COUNT 4

// measured distances in mm for rigid body
float rigid_points[RIGID_MARKER_COUNT][3] = {
  {0,0,0},
  {50,26,0},
  {90,30,0},
  {130,0,0}
};

struct RigidBodyDefApp : App {

  Phasespace *tracker;

  RigidBodyDefApp(){

    // create and get Phaspace master instance
    tracker = Phasespace::master();

    // create rigid body definition and assign some points
    RigidDef *rigid = new RigidDef();
    for(int i = 0; i < RIGID_MARKER_COUNT; i++)
      rigid->markers[i] = rigid_points[i];

    tracker->addRigid( rigid );

    // start tracker data listener thread
    tracker->start(); // use to connect to Tracking system while in the Allosphere and record data

    nav().pos(0,0,5);
    
    ViewpointWindow* win = initWindow(Window::Dim(700, 700));
    start();
  }
  
  virtual void onAnimate(double dt) {}

  virtual void onDraw(Graphics& g, const Viewpoint& v) {

    g.antialiasing(Graphics::NICEST);

    DebugDraw::drawAlloSphere(g);
    DebugDraw::drawMarkers(g);
    DebugDraw::drawRigid(g,0);
  }

};

// Make an app
//
RigidBodyDefApp app;
