/*
Allocore Example: Phasespace Pickable Test

Description:
This example demonstrates how associate a mesh with a pickable
and interact with it via phasespace gloves

Author:
Tim Wood, March 2017
*/

#include "allocore/io/al_App.hpp"
#include "allocore/ui/al_BoundingBox.hpp"
#include "allocore/ui/al_Pickable.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/DebugDraw.hpp"

using namespace al;

class PickableTest : public App, Phasespace::Listener {
public:

  bool loadedFont;
  double t;
  Light light;      
  Material material; 
  
  Mesh mesh; 

  Pickable pickable;
  Font font;

  Rayd sceneRayL;

  PickableTest(){

    Phasespace::master()->addListener(*this)->startPlaybackFile("Phasespace/marker-data/gloves.txt");
    // Phasespace::master()->addListener(*this)->start();

    // try to load font
    loadedFont = font.load("AlloSystem/allocore/share/fonts/VeraMono.ttf", 72);
    if(!loadedFont) std::cout << "Failed to load font. Not rendering labels.." << std::endl;

    // position camera, disable mouse to look
    nav().pos(0,0,5);
    navControl().useMouse(false);

    // Create a red spheres mesh
    addSphere(mesh,1);
    mesh.translate(-3,3,0);
    addSphere(mesh,2);
    mesh.translate(-2.7,2.7,0);
    addSphere(mesh,0.7);
    mesh.translate(2.7,-2.7,0);
    mesh.scale(0.5);

    mesh.generateNormals();
    mesh.color(RGB(1,0,0));

    // Initialize BoundingBox
    pickable.set(mesh);
    pickable.bb.glUnitLength = 10;
    pickable.pose.pos(0,0,-5);

    // create window
    initWindow();
  }

  void onAnimate(double dt){
    // move light in a circle
    t += dt;
    light.pos(10*cos(t),0,10*sin(t));
  }

  void onDraw(Graphics& g){

    // draw lit mesh
    g.lighting(true);
    light();
    material();
    pickable.drawMesh(g);
    
    g.lighting(false);
    pickable.drawBB(g);

    if(loadedFont){
      g.blendAdd();
      g.color(1,1,1);
      pickable.drawLabels(g, font, nav());
      g.blendOff();
    }

    DebugDraw::drawAlloSphere(g);
    DebugDraw::drawGloves(g);

    // draw left glove pick ray
    Mesh &m = g.mesh();
    m.reset();
    m.primitive(g.LINES);
    m.vertex(sceneRayL.o);
    m.vertex(sceneRayL.o + sceneRayL.d*5.8);
    g.color(1,0,0);
    g.draw(m); 

  }

  virtual void onPhasespaceEvent( const Phasespace::Event &event){

    Nav fakeNav;
    Rayd r = event.getPickRayLeft(fakeNav);
    Glove l = event.leftGlove;

    if(l.pinchOn[eIndex]){ // pinch started
      pickable.pick(r);
    } else if(l.pinched[eIndex]){ // currently pinched
      pickable.drag(r);
    } else if(l.pinchOff[eIndex]){ // pinch finished
      pickable.unpick(r);
    } else { // not pinched
      pickable.point(r);
    }    

    sceneRayL = r;
  }

  virtual void onMouseMove(const ViewpointWindow& w, const Mouse& m){
    // make a ray from mouse location
    Rayd r = getPickRay(w, m.x(), m.y());
    pickable.point(r);
  }
  virtual void onMouseDown(const ViewpointWindow& w, const Mouse& m){
    Rayd r = getPickRay(w, m.x(), m.y());
    pickable.pick(r);
  }
  virtual void onMouseDrag(const ViewpointWindow& w, const Mouse& m){
    Rayd r = getPickRay(w, m.x(), m.y());
    pickable.drag(r);
  }
  virtual void onMouseUp(const ViewpointWindow& w, const Mouse& m){
    Rayd r = getPickRay(w, m.x(), m.y());
    pickable.unpick(r);
  }
};

int main(){
  PickableTest().start();
}
