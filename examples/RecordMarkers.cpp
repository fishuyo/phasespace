/*
 * RecordMarkers.cpp - record marker data for use in development and testing.
 *      press 'r' to start recording, and 't' to stop
 *
 * Tim Wood 2013 
 * */

#include <iostream>
#include "allocore/al_Allocore.hpp"
#include "allocore/io/al_App.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/Glove.hpp"
#include "phasespace/DebugDraw.hpp"

int main() {}

using namespace std;
using namespace al;

struct RecordMarkersApp : App, Phasespace::Listener {

  Phasespace *tracker;

  RecordMarkersApp(){

    // create and get Phaspace master instance
    tracker = Phasespace::master();
    tracker->addListener(*this);

    // start tracker data listener thread
    // tracker->start(); // use to connect to Tracking system while in the Allosphere and record data
    tracker->startPlaybackFile("Phasespace/marker-data/gloves.txt"); // use to playback recorded data

    nav().pos(0,0,5);

    ViewpointWindow* win = initWindow(Window::Dim(700, 700));
    start();
  }

  virtual void onPhasespaceEvent(const Phasespace::Event &event){}

  virtual void onKeyDown(const ViewpointWindow& w, const Keyboard& k){
    switch( k.key() ){
      case 'r': 
        cout << "recording markers starting.." << endl;
        tracker->startRecordingMarkers("Phasespace/marker-data/marker000.txt"); 
        break;
      case 't': 
        cout << "recording stopped." << endl;
        tracker->stopRecordingMarkers();
        break;
      default: break;
    }
  }
  
  virtual void onAnimate(double dt) {}

  virtual void onDraw(Graphics& g, const Viewpoint& v) {
    g.antialiasing(Graphics::NICEST);

    DebugDraw::drawAlloSphere(g);
    DebugDraw::drawGloves(g);
    DebugDraw::drawMarkers(g);
  }
};

// Make an app
//
RecordMarkersApp app;
