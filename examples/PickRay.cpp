/*
 * PickRay.cpp - how to get a ray to do picking in the Allosphere from glove data
 *
 * Tim Wood 2015
 * */

#include <iostream>
#include "allocore/al_Allocore.hpp"
#include "allocore/io/al_App.hpp"

#include "phasespace/Phasespace.hpp"
#include "phasespace/Glove.hpp"
#include "phasespace/DebugDraw.hpp"

int main() {}

using namespace std;
using namespace al;

struct PickRayApp : App, Phasespace::Listener {

  Phasespace *tracker;

  Rayd realRayL, realRayR;
  Rayd sceneRayL, sceneRayR;

  PickRayApp(){

    // create and get Phaspace master instance
    tracker = Phasespace::master();
    tracker->addListener(*this);

    // start tracker data listener thread
    // tracker->start();                                     // use this call when in sphere
    tracker->startPlaybackFile("Phasespace/marker-data/gloves.txt"); // playback recorded marker data

    nav().pos(0,0,5);

    initWindow(Window::Dim(700, 700));
    start();
  }

  virtual void onPhasespaceEvent(const Phasespace::Event &event){

    Vec3f headPos = event.headPosition;
    realRayL = Rayd(headPos, event.leftGlove.centroid - headPos ); // ray pointing from head to left hand
    realRayR = Rayd(headPos, event.rightGlove.centroid - headPos ); // ray pointing from head to right hand

    // given a nav, transforms our real space ray into a ray you can use in your virtual scene
    Nav fakeNav;
    sceneRayL = tracker->getSpherePickRay(fakeNav, realRayL);
    sceneRayR = tracker->getSpherePickRay(fakeNav, realRayR);

    // convenience methods for doing the above
    Rayd sceneRayL = event.getPickRayLeft(fakeNav);
    // Rayd sceneRayR = event.getPickRayRight(fakeNav);

  }

  virtual void onAnimate(double dt) {}

  virtual void onDraw(Graphics& g, const Viewpoint& v) {

    g.antialiasing(Graphics::NICEST);

    DebugDraw::drawAlloSphere(g);
    DebugDraw::drawGloves(g);
    DebugDraw::drawMarker(g,17); // head led

    // draw rays
    Mesh &m = g.mesh();
    m.reset();
    m.primitive(g.LINES);
    m.vertex(realRayL.o);
    m.vertex(realRayL.o + realRayL.d*5.8);
    m.vertex(realRayR.o);
    m.vertex(realRayR.o + realRayR.d*5.8);
    g.color(1,0,0);
    g.draw(m);    

    m = g.mesh();
    m.reset();
    m.primitive(g.LINES);
    m.vertex(sceneRayL.o);
    m.vertex(sceneRayL.o + sceneRayL.d*5.8);
    m.vertex(sceneRayR.o);
    m.vertex(sceneRayR.o + sceneRayR.d*5.8);
    g.color(1,0,1);
    g.draw(m);
  }
};

// Make an app
//
PickRayApp app;
