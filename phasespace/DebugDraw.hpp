
#ifndef DEBUGGRAPHICS_H
#define DEBUGGRAPHICS_H

#include "allocore/graphics/al_Graphics.hpp"
#include "allocore/graphics/al_Mesh.hpp"

#include "Phasespace.hpp"
#include "Glove.hpp"

struct DebugDraw {

  static void drawMarker(Graphics& g, int id){
    Phasespace *ps = Phasespace::master();
    g.begin(g.POINTS);
    g.color(1,0,0);
    if(ps->markerNumberOfAbsentFrames[id] == 0) 
      g.vertex(ps->markerPositions[id]);
    g.end();
  }

  static void drawMarkers(Graphics& g){
    Phasespace *ps = Phasespace::master();
    g.begin(g.POINTS);
    g.color(1,0,0);
    for(int i = 0; i < MARKER_COUNT; i++){
      if(ps->markerNumberOfAbsentFrames[i] == 0) 
        g.vertex(ps->markerPositions[i]);
    }
    g.end();
  }

  static void drawRigid(Graphics& g, int id){
    Phasespace *ps = Phasespace::master();

    // draw rigid axes if seen
    if( ps->rigidNumberOfAbsentFrames[id] == 0){ 
      Pose &p = ps->rigidPoses[id];

      g.pointSize(7.f);
      g.color(0.f,1.f,0.f);
      g.begin(Graphics::POINTS);
        g.vertex(p.pos());
      g.end();

      g.begin(Graphics::LINES);
        g.color(1.f,0.f,0.f);
        g.vertex(p.pos());
        g.vertex(p.pos() + p.ur());
        g.color(0.f,1.f,0.f);
        g.vertex(p.pos());
        g.vertex(p.pos() + p.uu());
        g.color(0.f,0.f,1.f);
        g.vertex(p.pos());
        g.vertex(p.pos() - p.uf());
      g.end();
    }
  }

  static void drawGlove(Graphics& g, Glove& glove){
    g.pointSize(5.f);
    g.lineWidth(2.f);

    Vec3f color(0,0,1);

    //finger / pinch points and vectors
    for(int i=0; i < 4; i++){
      if( glove.pinched[i] ){
        g.color(0.f,1.f,0.f);
        g.begin(Graphics::POINTS);
        g.vertex(glove.pos[i]);
        g.pointSize(5.f);
        g.vertex(glove.pinchPos[i]);
        g.end();
        g.pointSize(5.f);
        g.begin(Graphics::LINES);
        g.vertex(glove.pos[i]);
        g.vertex(glove.pinchPos[i]);
        g.end();

      } else if( glove.seen[i]){
        g.color(color);
        g.begin(Graphics::POINTS);
        g.vertex(glove.pos[i]);
        g.end();
        
        g.lineWidth(2.f);
        if(i<2 && glove.seen[eBackPinkySide]){
          g.begin(Graphics::LINES);
          g.vertex(glove.pos[i]);
          g.vertex(glove.pos[eBackPinkySide]);
          g.end();
        }else if( glove.seen[eBackThumbSide]){
          g.begin(Graphics::LINES);
          g.vertex(glove.pos[i]);
          g.vertex(glove.pos[eBackThumbSide]);
          g.end();  
        }
      }

    }

    // rest of glove points
    for(int i=4; i<8; i++){
      if( !glove.seen[i] ) continue;
      g.color(color);
      g.begin(Graphics::POINTS);
      g.vertex(glove.pos[i]);
      g.end();
    }

    // thumb and back of hand vectors
    g.color(color);
    if( glove.seen[eThumbTip] && glove.seen[eThumbBase]){
      g.begin(Graphics::LINES);
      g.vertex(glove.pos[eThumbTip]);
      g.vertex(glove.pos[eThumbBase]);
      g.end();
    }
    if( glove.seen[eBackThumbSide] && glove.seen[eBackPinkySide]){
      g.begin(Graphics::LINES);
      g.vertex(glove.pos[eBackThumbSide]);
      g.vertex(glove.pos[eBackPinkySide]);
      g.end();
    }

    // centroid
    g.color(1.f,0.f,0.f);
    g.begin(Graphics::POINTS);
    g.vertex(glove.centroid);
    g.end();
    g.lineWidth(1.f);
  }
  
  static void drawGloves(Graphics &g){
    Phasespace *ps = Phasespace::master();
    drawGlove(g, ps->event.leftGlove);
    drawGlove(g, ps->event.rightGlove);
  }

  static void drawAlloSphere(Graphics &g){
    Mesh sphere;
    Mesh bridge;
    addSphere(sphere, 5.8);
    addSurface(bridge,24,4, 12, 2);

    g.polygonMode(Graphics::LINE);
    g.pushMatrix();
    g.color(0.5,0.5,0.5);
    g.draw(sphere);
    g.translate(0,-1.5,0);
    g.rotate(90,1,0,0);
    g.draw(bridge);
    g.popMatrix();
    g.polygonMode(Graphics::FILL);
  }
  
};

#endif