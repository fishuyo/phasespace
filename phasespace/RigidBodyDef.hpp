#ifndef _RIGIDBODYDEF_
#define _RIGIDBODYDEF_

#include "allocore/io/al_File.hpp"

#include <map>
#include <fstream>
#include <sstream>

struct Marker {
    Marker(int id_, float x_, float y_, float z_) : id(id_), x(x_), y(y_), z(z_) {}
    int id;
    float x,y,z;
};

struct RigidDef {
    bool active;
    int tracker;
    std::map<int, float*> markers;

    RigidDef(){};

    void save(std::string& name){
        al::File f = al::File(name, "rw");
        std::stringstream ss;
        for (std::map<int,float*>::iterator itr = markers.begin(); itr != markers.end(); ++itr){
            int idx = itr->first;
            float *pos = itr->second;
            ss << idx << " " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
        }
        f.write( ss.str() );
        f.close();
    };
    void load(std::string& name){
        //TODO
    };
};

#endif